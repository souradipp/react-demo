import React from 'react'

function Header() {
    return (
        <>

            <header>
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="#"><b>DEMO LOGO</b></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav  justify-content-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Plans & Subscription</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Experiece Demo</a>
                                </li>
                            </ul>
                            <div class="form-inline my-2 my-lg-0 ml-auto">
                                <a class="btn btn-outline-success my-2 my-sm-0"><i class="fa fa-user-circle" aria-hidden="true"></i> User</a>
                                <a class="btn btn-outline-success my-2 my-sm-0"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </header>
        </>
    )
}

export default Header
