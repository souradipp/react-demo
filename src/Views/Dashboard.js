import React from 'react'

import Header from '../layouts/Header'
import Banner from '../Components/Banner'
import Menu from '../Components/Menu'

function Dashboard() {
    return (
        <>
            <Header />
            <main>
                <Banner />
                <Menu />
            </main>
        </>
    )
}

export default Dashboard
