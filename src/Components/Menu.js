import React from 'react'
import Dropdown from './Dropdown'

function Menu() {
    return (
        <>
            <section class="b-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <Dropdown />
                        </div>
                        <div class="col-lg-3">
                            <Dropdown />
                        </div>
                        <div class="col-lg-3">
                            <Dropdown />
                        </div>
                        {/* <div class="col-lg-3">
                            <figure class="add-btn"><img src="img/img.png" alt="" /></figure>
                        </div> */}
                    </div>
                </div>
            </section>
        </>
    )
}

export default Menu
