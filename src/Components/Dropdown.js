import React from 'react'

function Dropdown() {
    return (
        <div class="dropdown">
            <button class="btn dropdown-toggle" type="button" id="dropdownA" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Menu A</button>
            <div class="dropdown-menu" aria-labelledby="dropdownA">
                <a class="dropdown-item" href="#">A</a>
                <a class="dropdown-item" href="#">B</a>
                <a class="dropdown-item" href="#">C</a>
            </div>
        </div>
    )
}

export default Dropdown
