import React from 'react'

function CollapseMenu() {
    return (
        <>
            <div class="dash-rght h-100">
                <a href="#" data-toggle="collapse" data-target="#demo" class="collapse-btn"><span><i class="fa fa-angle-double-left" aria-hidden="true"></i></span>Collapse</a>
                <div id="demo" class="collapse">
                    <ul>
                        <li>
                            <a href="#">
                                <figure><img src="img/image.jpg" alt="" /></figure>
                                <span>Diana</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><img src="img/image.jpg" alt="" /></figure>
                                <span>Diana</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><img src="img/image.jpg" alt="" /></figure>
                                <span>Diana</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><img src="img/image.jpg" alt="" /></figure>
                                <span>Diana</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><img src="img/image.jpg" alt="" /></figure>
                                <span>Diana</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><img src="img/image.jpg" alt="" /></figure>
                                <span>Diana</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <figure><img src="img/image.jpg" alt="" /></figure>
                                <span>Diana</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </>
    )
}

export default CollapseMenu
