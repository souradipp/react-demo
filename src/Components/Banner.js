import React, { useState } from 'react'
import CollapseMenu from './CollapseMenu'

function Banner() {
    let [noOfImage, setNoOfImage] = useState(1);
    let [noOfImageData, setNoOfImageData] = useState([noOfImage]);

    const addImage = () => {
        setNoOfImage(noOfImage++);
        setNoOfImageData([...noOfImageData, noOfImage])
    }

    const getDisplayClass = (index) => {
        let len = noOfImageData.length;
        console.log(len, index)

        if (index == len && len % 3 == 1) {
            // return 'col-md-6'
            return 'col-md-12'
        }
        else {
            return 'col-md-4'
        }
    }





    return (
        <>
            <section class="main-banner">
                <div class="container h-100">
                    <div class="row h-100 custom-row">
                        <div class="col-lg-12 column">
                            <div class="dash-lft">
                                <div class="row react-img">
                                    {/* <div class="col-md-6">
                                        <div class="img-box">
                                            <img src="img/girl.jpg" alt="" />
                                            <a href="img/girl.jpg" class="fancybox" data-fancybox><span><img src="img/expand.svg" alt="" /></span></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="img-box">
                                            <img src="img/girl.jpg" alt="" />
                                            <a href="img/girl.jpg" class="fancybox" data-fancybox><span><img src="img/expand.svg" alt="" /></span></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="img-box">
                                            <img src="img/girl.jpg" alt="" />
                                            <a href="img/girl.jpg" class="fancybox" data-fancybox><span><img src="img/expand.svg" alt="" /></span></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="img-box">
                                            <img src="img/girl.jpg" alt="" />
                                            <a href="img/girl.jpg" class="fancybox" data-fancybox><span><img src="img/expand.svg" alt="" /></span></a>
                                        </div>
                                    </div> */}
                                    {
                                        noOfImageData.map((data, index) => {
                                            return <>
                                                <div className={getDisplayClass(index + 1)}>
                                                    <div class="img-box">
                                                        <img src="img/girl.jpg" alt="" />
                                                        <a href="img/girl.jpg" class="fancybox" data-fancybox><span><img src="img/zoom-directions.svg" alt="" /></span></a>
                                                    </div>
                                                </div>
                                            </>
                                        })
                                    }
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3 add-btn-right">
                            <figure class="add-btn"><img onClick={addImage} src="img/image.png" alt="" /></figure>
                        </div>
                    </div>
                </div>
            </section>


        </>
    )
}

export default Banner
